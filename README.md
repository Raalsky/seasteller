# NOAA Competition Participaiting - MLSG [@UJ](http://www.uj.edu.pl/)
## NOAA Fisheries Steller Sea Lion Population Count Kaggle Competition

Steller sea lions in the western Aleutian Islands have declined 94 percent in the last 30 years. The endangered western population, found in the North Pacific, are the focus of conservation efforts which require annual population counts. Specially trained scientists at NOAA Fisheries Alaska Fisheries Science Center conduct these surveys using airplanes and unoccupied aircraft systems to collect aerial images. Having accurate population estimates enables us to better understand factors that may be contributing to lack of recovery of Stellers in this area.

Currently, it takes biologists up to four months to count sea lions from the thousands of images NOAA Fisheries collects each year. Once individual counts are conducted, the tallies must be reconciled to confirm their reliability. The results of these counts are time-sensitive.

In this competition, Kagglers are invited to develop algorithms which accurately count the number of sea lions in aerial photographs. Automating the annual population count will free up critical resources allowing NOAA Fisheries to focus on ensuring we hear the sea lion’s roar for many years to come. Plus, advancements in computer vision applied to aerial population counts may also greatly benefit other endangered species.

### Submitions
The aim of the competition is to count each type of steller sea lion in each photo. See the Data tab for more details.

Your submission file should have the following format:
```
test_id,adult_males,subadult_males,adult_females,juveniles,pups
0,1,1,1,1,1
1,1,1,1,1,1
2,1,1,1,1,1
etc
```
Your submissions will be evaluated by their RMSE from the human-labelled ground truth, averaged over the columns.

### Prizes
* 1st place - $12,000
* 2nd place - $8,000
* 3rd place - $5,000
