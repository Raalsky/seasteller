import numba
import numpy as np
from tqdm import tqdm
from imageutils import *
import cv2
import math
import json

# System directories
from os import listdir
from os.path import isfile, join
from os import environ as env

from keras.preprocessing.image import img_to_array

# Generate count-sum map for input coordinates
@numba.jit
def computeCountsMap(coords, height, width, CLASSES):
    output = np.zeros((height, width, len(CLASSES)), dtype=np.uint16)

    # Points marking
    for class_id in enumerate(CLASSES):
        for y, x in coords[class_id[1]]:
            output[y][x][class_id[0]] = 1

    # Presums
    for x in range(1, width):
        output [0][x] += output[0][x-1]
    for y in range(1, height):
        output [y][0] += output[y-1][0]
        for x in range(1, width):
            output [y][x] += output [y-1][x] + output[y][x-1] - output[y-1][x-1]
    return output

# Read image, get resolution and generate count-sum map
def processImage(dataset, id, classes):
    # Get sample paths
    id, img_path, coords_path = dataset[id]

    # Read image
    img = bgr2rgb(cv2.imread(img_path))

    # Read coordinates
    with open(coords_path) as coords_file:
        coords = json.load(coords_file)

    # Get resolution
    height, width = resolution(img)

    # Create counts map
    cmap = computeCountsMap(coords, height, width, classes)

    return img, height, width, cmap

# Sample patches from input images
@numba.jit
def getPatches(img, height, width, cmap=None, sample_size=150, patch_size=150, stride=50, focused_class=0, classes={}, margin=8):
    numberOfSamples = 15 * math.ceil(float(height)/float(patch_size)) * math.ceil(float(width)/float(patch_size))
    patches = np.zeros((1*numberOfSamples, sample_size, sample_size, 3), dtype=np.float32)
    labels = np.zeros((1*numberOfSamples, len(classes)), dtype=np.uint8)
    i = 0
    for y in range(patch_size, height, stride):
        for x in range(patch_size, width, stride):
            x = min(x, width)
            y = min(y, height)
            patches[i] = img_to_array(img [y - patch_size : y, x - patch_size : x])
            labels [i,:] = cmap[y - margin, x - margin,:] - cmap[y - margin, x - patch_size + margin,:] - cmap[y - patch_size + margin, x - margin,:] + cmap[y - patch_size + margin, x - patch_size + margin,:]
            if np.max(labels [i]) > 0:
                i += 1
            elif np.max(labels [i]) == 0 and np.random.randint(0, 100) == 0:
                i += 1
    return patches[:i], labels[:i]

# Return list generated patches with coresponding labels
def getSamples(dataset, ids, sample_size, patch_size, stride, focused_class, classes, save_patches=False, patches_path=None):
    samples = np.array([], dtype=np.float32)
    outs = np.array([])
    for id in tqdm(ids):
        #if id < 252:
        #   continue
        patches, labels = getPatches(*processImage(dataset, id, classes), sample_size, patch_size, stride, focused_class, classes)
        # Save patches
        if save_patches:
            for pid, patch in tqdm(enumerate(patches)):
                cv2.imwrite(join(patches_path, str(id) + '_' + str(pid) + '_' + str(labels[pid][0]) + '_' + str(labels[pid][1]) + '_' + str(labels[pid][2]) + '_' + str(labels[pid][3]) + '_' + str(labels[pid][4]) + '.jpg'), rgb2bgr(patch))
        else:
            if not samples.size:
                samples = np.resize(patches, (patches.shape[0], patches.shape[1], patches.shape[2], patches.shape[3]))
                outs = labels
            else:
                samples = np.append(samples, patches, axis=0)
                outs = np.append(outs, labels, axis=0)

    return samples, outs

# Shuffle dataset and labels
def randomize(dataset, labels):
    # Generate the permutation index array.
    permutation = np.random.permutation(dataset.shape[0])
    # Shuffle the arrays by giving the permutation in the square brackets.
    shuffled_a = dataset[permutation]
    shuffled_b = labels[permutation]
    return shuffled_a, shuffled_b

def getPatchesPadding(img, samples_size, patch_size, stride):
    h, w = resolution(img)
    height = h
    width = w
    if samples_size == 224:
        height = 16 + h + 16
        width  = 80 + w + 80
        padded = np.zeros((height, width,3))
        padded[16:16+h,80:80+w,:] = img
        img = padded
    patches = np.zeros((2000, 224, 224, 3))
    i = 0

    #print(h, w, height, width)

    for y in range(patch_size, height - patch_size, stride):
        for x in range(patch_size, width - patch_size, stride):
            x = min(x, width)
            y = min(y, height)
            patches[i] = img_to_array(img [y - patch_size : y, x - patch_size : x])
            i+=1

    return patches
