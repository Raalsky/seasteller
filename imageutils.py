# Built-in modules
import fractions
import operator
from io import BytesIO

# 3rd-party modules
import numpy as np
import cv2
from cv2 import imread
import PIL
from PIL import Image
from IPython.display import display, Image


# Get resolution of image
def resolution(img):
    # Numpy array image <numpy.ndarray> (width, height, ...)
    if isinstance(img, np.ndarray):
        return img.shape [ :2 ]
    # PIL Image <PIL.Image.Image>
    if isinstance(img, PIL.Image.Image):
        return img.size
    # Unknow type
    else:
        raise TypeError('Unknow image type. Only available types is numpy.ndarray. Yours is %s' % (type( img )))

# Get ratio of image
def ratio(img):
    res             = resolution( img )
    common_divisor  = fractions.gcd( *res )
    ratio           = tuple( ( int( res [ 0 ] / common_divisor ) , int( res [ 1 ] / common_divisor ) ) )

    return '%s:%s' % ratio

# Convert OpenCV image from BGR TO RGB
def bgr2rgb(img):
    b,g,r = cv2.split(img)
    return cv2.merge([r,g,b])

# Convert OpenCV image from RGB TO BGR
def rgb2bgr(img):
    r, g, b = cv2.split(img)
    return cv2.merge([b, g, r])

# Array to image
def array2img(img):
    img         = PIL.Image.fromarray(img)
    byteForm    = BytesIO()

    # Save image to bytes
    img.save( byteForm, format='JPEG' )

    return Image(byteForm.getvalue(), format='jpg')

# Display image in cell
def imshow(img):
    # Numpy array image
    if isinstance(img, np.ndarray):
        # Convert to uint8
        #if len(img.shape) != 3 or img.shape[2] == 1:
        #    tmp = np.resize(img, (img.shape[0], img.shape[1], 3))
        #    tmp[:,:,0] = img[:,:,0]
        #    tmp[:,:,1] = img[:,:,0]
        #    tmp[:,:,2] = img[:,:,0]
        #    img = tmp
        #img = np.asarray(img, np.uint8)
        # Create images from ndarray
        img = array2img(img)

    # Display image
    display(img)

# Bilinear interpolation resizing
def resize(img):
    pass

def colInRange(color, minColor, maxColor):
    r, g, b = color
    mR, mG, mB = minColor
    MR, MG, MB = maxColor

    if r >= mR and r <= MR:
        if g >= mG and g <= MG:
            if b >= mB and b <= MB:
                return True
    return False

if __name__ == "__main__":
    print( imread( 'dataset/TrainSmall2/Train/42.jpg' ).shape )
    print( resolution( np.zeros( (256, 128, 3) ) ) )
    print( resolution( cv2.imread( 'dataset/TrainSmall2/Train/42.jpg' ) ) )
    print( resolution( PIL.Image.fromarray( cv2.imread( 'dataset/TrainSmall2/Train/42.jpg' ) ) ) )

    print( ratio( np.zeros( (256, 128, 3) ) ) ) # Expected: '2:1'
    print( ratio( imread('dataset/TrainSmall2/Train/42.jpg') ) )

    imshow( imread('dataset/TrainSmall2/Train/42.jpg') )
