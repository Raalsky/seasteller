"""
Launching, training and evaluating selected models with Keras
    @author : Raalsky
    @date   : 2017-05-03
"""

# Command-Line arguments parsing
from session_arguments import *

# Data Science & Machine Learning
import cv2
import numpy as np
import pandas as pd

# Private utils
from imageutils import *
from datautils import *

# System directories
from os import listdir
from os.path import isfile, join
from os import environ as env

# Multiprocessing
from multiprocessing import Pool

# ProgressBar
from tqdm import tqdm, tqdm_notebook

# Python miscellaneous libraries
from time import time
import json, glob, numba
import collections
import math

# Keras library
import keras
from keras.utils import to_categorical

# Model loading
from keras import backend as K
from keras.models import Model
from keras import optimizers
from keras import metrics
from keras.preprocessing import image
from keras.models import load_model
from keras.preprocessing.image import *

from importlib import import_module

from rmse import RMSE

# ------------------------------ DATA GENERATORS -------------------------------
class PoisonDirectoryIterator(DirectoryIterator):
    def __init__(self, directory, image_data_generator, num_columns=5,
                 target_size=(256, 256), color_mode='rgb',
                 batch_size=32, shuffle=True, seed=None,
                 data_format=None,
                 follow_links=False):
        
        if data_format is None:
            data_format = K.image_data_format()
        self.directory = directory
        self.image_data_generator = image_data_generator
        self.target_size = tuple(target_size)
        self.num_columns=num_columns
        if color_mode not in {'rgb', 'grayscale'}:
            raise ValueError('Invalid color mode:', color_mode,
                             '; expected "rgb" or "grayscale".')
        self.color_mode = color_mode
        self.data_format = data_format
        if self.color_mode == 'rgb':
            if self.data_format == 'channels_last':
                self.image_shape = self.target_size + (3,)
            else:
                self.image_shape = (3,) + self.target_size
        else:
            if self.data_format == 'channels_last':
                self.image_shape = self.target_size + (1,)
            else:
                self.image_shape = (1,) + self.target_size

        white_list_formats = {'png', 'jpg', 'jpeg', 'bmp'}

        # first, count the number of samples and classes
        self.samples = 0

        def _recursive_list(subpath):
            return sorted(os.walk(subpath, followlinks=follow_links), key=lambda tpl: tpl[0])

        subpath = directory
        for root, _, files in _recursive_list(subpath):
            for fname in files:
                is_valid = False
                for extension in white_list_formats:
                    if fname.lower().endswith('.' + extension):
                        is_valid = True
                        break
                if is_valid:
                    self.samples += 1
        print('Found %d images' % (self.samples))

        # second, build an index of the images in the different class subfolders
        self.filenames = []
        self.labels = np.zeros((self.samples,num_columns), dtype='int32')
        i = 0
        for root, _, files in _recursive_list(directory):
            for fname in files:
                is_valid = False
                for extension in white_list_formats:
                    if fname.lower().endswith('.' + extension):
                        is_valid = True
                        break
                if is_valid:
                    #print(fname)
                    ys = fname.split('.')[0].split('_')[2:]
                    #print(ys)
                    #print(num_columns)
                    for j, y in enumerate(ys):
                        #print(int(y))
                        self.labels[i][j] = int(y)
                    i += 1
                    # add filename relative to directory
                    absolute_path = os.path.join(root, fname)
                    self.filenames.append(os.path.relpath(absolute_path, directory))
        
        #print(self.labels)
        #print(self.filenames)
        super(DirectoryIterator, self).__init__(self.samples, batch_size, shuffle, seed)
    
    def next(self):
        """For python 2.x.
        # Returns
            The next batch.
        """
        with self.lock:
            index_array, current_index, current_batch_size = next(self.index_generator)
        #print(index_array)
        # The transformation of images is not under thread lock
        # so it can be done in parallel
        #print("num_columns: %d" % (self.num_columns))
        batch_x = np.zeros((current_batch_size,) + self.image_shape, dtype=K.floatx())
        batch_y = np.zeros((current_batch_size, self.num_columns), dtype=K.floatx())
        grayscale = self.color_mode == 'grayscale'
        # build batch of image data
        for i, j in enumerate(index_array):
            fname = self.filenames[j]
            #print(fname)
            #print(self.labels[j])
            for u_i, u_v in enumerate(self.labels[j]):
                #print(u_i, u_v)
                batch_y[i][u_i] = u_v
            #print(batch_y[i])
            img = load_img(os.path.join(self.directory, fname),
                           grayscale=grayscale,
                           target_size=self.target_size)
            #imshow(img)
            x = img_to_array(img, data_format=self.data_format)
            x = self.image_data_generator.random_transform(x)
            x = self.image_data_generator.standardize(x)
            batch_x[i] = x
        # build batch of labels
        batch_y = self.labels[index_array]
        return batch_x, batch_y
    
class PoisonImageDataGenerator(ImageDataGenerator):
    def __init__(self,
                 featurewise_center=False,
                 samplewise_center=False,
                 featurewise_std_normalization=False,
                 samplewise_std_normalization=False,
                 zca_whitening=False,
                 zca_epsilon=1e-6,
                 rotation_range=0.,
                 width_shift_range=0.,
                 height_shift_range=0.,
                 shear_range=0.,
                 zoom_range=0.,
                 channel_shift_range=0.,
                 fill_mode='nearest',
                 cval=0.,
                 horizontal_flip=False,
                 vertical_flip=False,
                 rescale=None,
                 preprocessing_function=None,
                 data_format=None):
        if data_format is None:
            data_format = K.image_data_format()
        self.featurewise_center = featurewise_center
        self.samplewise_center = samplewise_center
        self.featurewise_std_normalization = featurewise_std_normalization
        self.samplewise_std_normalization = samplewise_std_normalization
        self.zca_whitening = zca_whitening
        self.zca_epsilon = zca_epsilon
        self.rotation_range = rotation_range
        self.width_shift_range = width_shift_range
        self.height_shift_range = height_shift_range
        self.shear_range = shear_range
        self.zoom_range = zoom_range
        self.channel_shift_range = channel_shift_range
        self.fill_mode = fill_mode
        self.cval = cval
        self.horizontal_flip = horizontal_flip
        self.vertical_flip = vertical_flip
        self.rescale = rescale
        self.preprocessing_function = preprocessing_function

        if data_format not in {'channels_last', 'channels_first'}:
            raise ValueError('data_format should be "channels_last" (channel after row and '
                             'column) or "channels_first" (channel before row and column). '
                             'Received arg: ', data_format)
        self.data_format = data_format
        if data_format == 'channels_first':
            self.channel_axis = 1
            self.row_axis = 2
            self.col_axis = 3
        if data_format == 'channels_last':
            self.channel_axis = 3
            self.row_axis = 1
            self.col_axis = 2

        self.mean = None
        self.std = None
        self.principal_components = None

        if np.isscalar(zoom_range):
            self.zoom_range = [1 - zoom_range, 1 + zoom_range]
        elif len(zoom_range) == 2:
            self.zoom_range = [zoom_range[0], zoom_range[1]]
        else:
            raise ValueError('zoom_range should be a float or '
                             'a tuple or list of two floats. '
                             'Received arg: ', zoom_range)
            
    def flow_from_directory(self, directory,
                            target_size=(256, 256), color_mode='rgb',
                            batch_size=32, shuffle=True, seed=None,
                            follow_links=False, num_columns=5):
        return PoisonDirectoryIterator(
            directory, self,
            target_size=target_size, color_mode=color_mode,
            data_format=self.data_format,
            batch_size=batch_size, shuffle=shuffle, seed=seed,
            follow_links=follow_links, num_columns=num_columns)

# ------------------------------------ CONSTANTS -------------------------------
DATASET_ROOT_PATH = env['STORAGE_PATH']

JPG_PATH = join(DATASET_ROOT_PATH, 'Train')
NUMPY_PATH = join(DATASET_ROOT_PATH, 'Numpy')
TEST_PATH = join(DATASET_ROOT_PATH, 'Test')

DATASET_PATH = JPG_PATH #NUMPY_PATH
DATASET_EXTENSION = '.jpg' #'.npy'
COORDS_PATH = join(DATASET_ROOT_PATH, 'Coords')
MODELS_PATH = 'models/'
PATCHES_PATH = join(DATASET_ROOT_PATH, 'patches/')

TRAIN_PATCHES = join(PATCHES_PATH, 'Train')
VALIDATE_PATCHES = join(PATCHES_PATH, 'Validate')

SEED = 2504
PATCH_SIZE = 150
SAMPLE_SIZE = 150
STRIDE = 150
MAX_COUNT = 96
FOCUSED_CLASS = 4
SAMPLES = 1
VALIDATION_SPLIT = 0.1

CLASSES = ['adult_males', 'subadult_males', 'adult_females', 'juveniles', 'pups']
# ------------------------------------ Main Logic ------------------------------

if __name__ == "__main__":
    # Get parameters from commandline
    p = Params().args

    # Set initial params
    EPOCHS = p.epochs
    SEED = p.seed
    STRIDE = p.stride
    MODEL_NAME = p.model
    SAMPLE_SIZE = p.sample_size
    BATCH_SIZE = p.batch_size
    PATCH_SIZE = SAMPLE_SIZE
    OPTIMISER = p.optimizer
    LEARNING_RATE = p.learning_rate
    LOSS = p.loss
    SAMPLES = p.samples
    PER_BATCH = p.per_batch

    # Set seed for random generator
    np.random.seed(SEED)

    # Get list of all sample images
    images_set = {int(f.partition('.')[0]) : join(DATASET_PATH, f) for f in listdir(DATASET_PATH) if f.endswith(DATASET_EXTENSION)}

    # Get list of all sample coordinates
    coords_set = {int(f.partition('.')[0]) : join(COORDS_PATH, f) for f in listdir(COORDS_PATH) if f.endswith('.csv')}

    # Get list of all test images
    test_set = {int(f.partition('.')[0]) : join(TEST_PATH, f) for f in listdir(TEST_PATH) if f.endswith(DATASET_EXTENSION)}

    # Merge images set with coresponding coordinates
    dataset_files = {}
    for entry in images_set.items():
        dataset_files [ entry[0] ] = tuple( [ entry [0] , images_set [ entry [0] ] , coords_set [ entry [0] ]] )

    # Extract ids
    test_files = {}
    for entry in test_set.items():
        test_files [ entry[0] ] = tuple( [ entry [0] , test_set [ entry [0] ]] )

    # Sort files by ID
    dataset_files = collections.OrderedDict(sorted(dataset_files.items()))
    print('Number of files in dataset:' , len(dataset_files))

    # Sort test by ID
    test_files = collections.OrderedDict(sorted(test_files.items()))
    print('Number of files in test set:' , len(test_files))

    # Load specific ids
    if not p.without_samples:
        samples, outs = getSamples(dataset_files, sorted(dataset_files.keys())[:SAMPLES], SAMPLE_SIZE, PATCH_SIZE, STRIDE, FOCUSED_CLASS, CLASSES, p.save_patches, PATCHES_PATH)
        print("Number of patches: ", len(samples))

    #labels = outs
    # Convert labels to one-shot vectors
    #if p.categorical:
    #    labels = np.zeros((len(outs), len(CLASSES)*MAX_COUNT), dtype=np.uint8)
    #    for id, label in enumerate(labels):
    #        labels[id] = np.concatenate(to_categorical(outs[id], MAX_COUNT))

    # Model to load
    model_name = MODEL_NAME
    model_file = join(MODELS_PATH, model_name, 'model.h5')

    print("SAMPLE_SIZE: %d, PATCH_SIZE: %d, STRIDE: %d" % (SAMPLE_SIZE, PATCH_SIZE, STRIDE))

    if not p.reset and isfile(model_file):
        # Load compiled model and weights
        print("Loading last model")
        model = load_model(model_file, custom_objects={'root_mean_squared_error':RMSE})
    else:
        # Load model structure
        print("Creating new model")
        mod = import_module('models.' + model_name + '.model')
        model = getattr(mod, 'model')

    if p.predict:
        # Predict sample image
        print("----------------------------")
        pred = np.array(model.predict(samples[:10]), dtype=np.uint8)#np.expand_dims(, axis=0)
        print('Predicted: ' ,pred)
        print('Ground-Truth: ', labels[:10])
        quit();

    if p.evaluate:
        print("------- Evaluating --------")
        #results = pd.DataFrame(0, index=np.arange(18636), columns=CLASSES)
        #results.fillna(0)
        #default = { 'adult_males' : 5,
        #            'subadult_males' : 4,
        #            'adult_females' : 26,
        #            'juveniles' : 15,
        #            'pups' : 11}
        #results.fillna(default)
        #for id, path in tqdm(test_files.values()):
        for id, path, coords in tqdm(dataset_files.values()):
            #if id <= 5000:
            #    continue
            #print(path)
            #img = bgr2rgb(cv2.imread(path))
            #patches = getPatchesPadding(img, 150, 150, 150)
            #print(model.predict(patches))
	    #pred = np.rint(model.predict(patches)).astype(int)
            #pred[pred < 0] = 0
            #pred = np.sum(pred, axis=0)
            #for i, classname in enumerate(CLASSES):
            #    results.set_value(id, classname, pred[i])
            #if id % 2 == 0:
            #    results.to_csv(join('results',MODEL_NAME + '_train.csv'))
            if id > 2:
                break
        #results.to_csv(join('results',MODEL_NAME + '_train.csv'))
        print("----------- End -----------")
        del model
        quit()

    # Model summary
    if p.summary:
        model.summary()

    # Compile model
    print("------- Compilling --------")
    model.compile(optimizer=optimizers.Adam(), loss=RMSE, metrics=['mae'])

    # Data Augumentation
    print("------- Data Augumentation --------")

    # Shuffle dataset
    #dataset_x, dataset_y = randomize(samples, labels)

    # Split dataset into train & test
    #TRAIN_SIZE = int(len(dataset_x) * (1-VALIDATION_SPLIT))
    #train_x, test_x = dataset_x[:TRAIN_SIZE], dataset_x[TRAIN_SIZE:]
    #train_y, test_y = dataset_y[:TRAIN_SIZE], dataset_y[TRAIN_SIZE:]

    # Validate
    datagen = PoisonImageDataGenerator()

    # Data Augumentation
    train_datagen = PoisonImageDataGenerator(
        rotation_range=10,
        #width_shift_range=0.2,
        #height_shift_range=0.2,
        horizontal_flip=True,
        fill_mode='nearest')

    # compute quantities required for featurewise normalization
    # (std, mean, and principal components if ZCA whitening is applied)
    #datagen.fit(samples)
    
    train_generator = train_datagen.flow_from_directory(directory=TRAIN_PATCHES, target_size=(150,150), color_mode='rgb', batch_size=32)
    validate_generator = datagen.flow_from_directory(directory=VALIDATE_PATCHES, shuffle=False, target_size=(150,150), color_mode='rgb', batch_size=32)

    # Stop before training
    if p.without_train:
        del model
        quit();

    print("------- Training initializing --------")

    # Inception preprocessing
    #from keras.applications.inception_v3 import preprocess_input
    #samples = preprocess_input(samples)

    # Save model after interval of epochs
    mc = keras.callbacks.ModelCheckpoint(model_file, monitor='val_loss', verbose=1, save_best_only=False, save_weights_only=False, mode='auto', period=1)

    # CSV Logger
    csv = keras.callbacks.CSVLogger(join(MODELS_PATH, model_name, 'training.log'))

    # Train current model
    print("------- Training started --------")
    try:
        # fits the model on batches with real-time data augmentation:
        model.fit_generator(train_generator, steps_per_epoch=3*3572, epochs=EPOCHS, validation_data=validate_generator, validation_steps=3*395, callbacks=[mc, csv])
        #model.fit_generator(datagen.flow(train_x, train_y, batch_size=32), steps_per_epoch=len(samples) / 32, epochs=EPOCHS, validation_data=(test_x, test_y), callbacks=[mc, csv])
    except KeyboardInterrupt:
        del model
        quit()
    finally:
        print("------- Training stopped --------")

    # Save model and weights
    model.save(model_file)

    # Clear memory allocated by model
    del model
