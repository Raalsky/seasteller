# Model George A
from keras.models import Model
from keras.layers import Input, Dense, Flatten, Conv2D, concatenate, GlobalAveragePooling2D
from keras.layers.convolutional import *

from keras.applications.inception_v3 import InceptionV3
from keras.applications.inception_v3 import preprocess_input

# Get back the convolutional part of a VGG network trained on ImageNet
inception = InceptionV3(
    include_top=False,
    weights='imagenet'
)

MAX_COUNT = 96

# Create custom input shape
input = Input(shape=(224,224,3), name = 'image_input')

# Use the generated model
output_inception = inception(input)

# Average the fully-connected layers
x = GlobalAveragePooling2D(name='avg_pool')(output_inception)
# adult_males
x1 = Dense(MAX_COUNT, activation='softmax', name='adult_males')(x)
# subadult_males
x2 = Dense(MAX_COUNT, activation='softmax', name='subadult_males')(x)
# adult_females
x3 = Dense(MAX_COUNT, activation='softmax', name='adult_females')(x)
# juveniles
x4 = Dense(MAX_COUNT, activation='softmax', name='juveniles')(x)
# pups
x5 = Dense(MAX_COUNT, activation='softmax', name='pups')(x)

output = concatenate([x1, x2, x3, x4, x5], name='output')

model = Model(inputs=input, outputs=output)
