# Model Lexi A
from keras.models import Model
from keras.layers import Input, Dense, Flatten, Conv2D, Conv1D, concatenate, GlobalAveragePooling2D
from keras.layers.convolutional import *

from keras.applications.inception_v3 import InceptionV3
from keras.applications.inception_v3 import preprocess_input

# Get back the convolutional part of a VGG network trained on ImageNet
inception = InceptionV3(
    include_top=False,
    weights='imagenet'
)

MAX_COUNT = 96

# Create custom input shape
input = Input(shape=(224,224,3), name = 'image_input')

# Use the generated model
x = inception(input)

# Average the fully-connected layers
x = GlobalAveragePooling2D(name='avg_pool')(x)

x = Dense(5, activation='linear', name='regressor')(x)

model = Model(inputs=input, outputs=x)
