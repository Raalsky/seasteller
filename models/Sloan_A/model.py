# Model Sloan A
from keras.models import Model
from keras.layers import Input, Dense, Flatten, Conv2D, Conv1D, concatenate, GlobalAveragePooling2D
from keras.layers.convolutional import *

from keras.applications.resnet50 import ResNet50
from keras.applications.resnet50 import preprocess_input

import sys
sys.setrecursionlimit(10000)

# Get back the convolutional part of a VGG network trained on ImageNet
resnet = ResNet50(
    include_top=False,
    weights='imagenet'
)

# Create custom input shape
input = Input(shape=(224,224,3), name = 'image_input')

# Use the generated model
x = resnet(input)

# Average the fully-connected layers
x = GlobalAveragePooling2D(name='avg_pool')(x)

x = Dense(5, activation='linear', name='regressor')(x)

model = Model(inputs=input, outputs=x)
