from keras.losses import *
from math import sqrt

# Custom loss, to be used by Keras
def root_mean_squared_error(y_true, y_pred):
    return K.sqrt(K.mean(K.square(y_pred - y_true), axis=-1))

rmse = RMSE = root_mean_squared_error
