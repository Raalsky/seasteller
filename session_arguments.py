import argparse

class Params:
    def __init__(self):
        parser = argparse.ArgumentParser(description='Model parameters')

        # Required
        parser.add_argument('--model', required=True, help='set model name to load')

        # Optional
        parser.add_argument('--seed', default=2501, type=int, help='set seed for random generator')
        parser.add_argument('--sample-size' , '-size', default=224, type=int, help='set size of cutted image samples')
        parser.add_argument('--stride', '-s', default=224, type=int, help='set stride for cutted image samples')
        parser.add_argument('--epochs', default=1, type=int, help='set number of training epochs')
        parser.add_argument('--batch-size', '-b', default=32, type=int, help='set number of batch size')
        parser.add_argument('--samples', default=1, type=int, help='specify how many samples will be processed')
        parser.add_argument('--per-batch', default=-1, type=int, help='specify how many samples will be loaded (batch of batches)')
        parser.add_argument('--save-patches', action='store_true', help='save patches to patches folder')
        parser.add_argument('--reset', action='store_true', help='disable loading last model weights if exists')
        parser.add_argument('--predict', action='store_true', help='predict sample image')
        parser.add_argument('--summary', action='store_true', help='show model summary')
        parser.add_argument('--categorical', action='store_true', help='convert labels to categorical')
        parser.add_argument('--without-train', action='store_true', help='stop before training start')
        parser.add_argument('--without-samples', action='store_true', help='stop loadning training files')
        parser.add_argument('--evaluate', action='store_true', help='evaluate test set')

        # Optimiser
        parser.add_argument('--optimizer', default='SGD', choices=['SGD', 'RMSprop'], help='set optimiser for training')
        parser.add_argument('--learning-rate', '-lr', default=0.00001, type=float, help='set learning rate')
        parser.add_argument('--loss', default='MSE', choices=['MSE', 'RMSE'])

        self.args = parser.parse_args()
